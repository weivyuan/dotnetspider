﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using DotnetSpider.Core;
using DotnetSpider.Core.Monitor;
using DotnetSpider.Core.Pipeline;
using DotnetSpider.Core.Processor;
using DotnetSpider.Core.Scheduler;
using DotnetSpider.Core.Selector;
using DotnetSpider.Demo.Entity;
using System.Drawing;
using System.Linq;
using Newtonsoft.Json;
using UTorrent.Api;
namespace DotnetSpider.Demo
{
    public class Avmoo
    {
        static List<avmovie> avMovies = new List<avmovie>();
        static List<avstar> avStars = new List<avstar>();
        static List<string> btlists = new List<string>();
        static string btlistjson = string.Empty;
        static bool downloadimage = false;
        static avmovie currentAVMovie = new avmovie();
        public static void Run(int start=0,int end=0)
        {
            if (start<=0)
            {
                Console.Write("请输入开始页码:");
                int.TryParse(Console.ReadLine(), out start);
            }
            
            
            if (end<=0)
            {
                Console.Write("请输入结束页码:");
                int.TryParse(Console.ReadLine(), out end);
            }
            

            // 定义要采集的 Site 对象, 可以设置 Header、Cookie、代理等
            var site = new Site {EncodingName = "UTF-8", RemoveOutboundLinks = true};
            for (int i = start; i <=end; i++)
            {
                site.AddStartUrl("https://" + $"avmo.pw/cn/released/page/{i}");

            }
            // 添加初始采集链接


            // 使用内存Scheduler、自定义PageProcessor、自定义Pipeline创建爬虫
            Spider spider =
                Spider.Create(site, new QueueDuplicateRemovedScheduler(), new MoviesPageProcessor())
                    .AddPipeline(new MovieListPipeline())
                    .SetThreadNum(1);
            spider.EmptySleepTime = 3000;
            spider.Deep = 2;
            spider.ThreadNum = 1;

            // 启动爬虫
            spider.Run();
        }

        public static void GetMovieInfo(string moviedetailurl)
        {
            var site = new Site {EncodingName = "UTF-8", RemoveOutboundLinks = true};
            site.AddStartUrl(moviedetailurl);
            Spider spider =
                Spider.Create(site, new QueueDuplicateRemovedScheduler(), new MoviesDetailProcessor())
                    .AddPipeline(new MovieDetailPipeline())
                    .SetThreadNum(1);
            spider.EmptySleepTime = 3000;
            spider.Deep = 2;
            spider.ThreadNum = 1;
            // 启动爬虫
            spider.Run();
        }
        
        public static void GetMagnetUrl(string url)
        {

            var site = new Site { EncodingName = "UTF-8", RemoveOutboundLinks = true };
            site.AddStartUrl(url);
            Spider spider =
                Spider.Create(site, new QueueDuplicateRemovedScheduler(), new MagnetDetailProcessor())
                    .AddPipeline(new MagenetDetailPipeline())
                    .SetThreadNum(1);
            spider.EmptySleepTime = 3000;
            spider.Deep = 2;
            spider.ThreadNum = 1;
            // 启动爬虫
            spider.Run();
        }

        public static void GetBTSOPW(string Code)
        {
            var site = new Site { EncodingName = "UTF-8", RemoveOutboundLinks = true };

            site.AddStartUrl("https://btso.pw/search/" + Code);


            // 添加初始采集链接


            // 使用内存Scheduler、自定义PageProcessor、自定义Pipeline创建爬虫
            Spider spider =
                Spider.Create(site, new QueueDuplicateRemovedScheduler(), new BTListProcessor())
                    .AddPipeline(new BTListPipeline())
                    .SetThreadNum(1);
            spider.EmptySleepTime = 3000;
            spider.Deep = 2;
            spider.ThreadNum = 1;
            
            // 启动爬虫
            spider.Run();
        }
        #region 采集后的处理
        private class BTListPipeline : BasePipeline
        {
            public override void Process(ResultItems resultItems)
            {
                btlists = new List<string>();
                List<string> bturls= resultItems.Results["BTURLS"];                
                foreach (var bturl in bturls)
                {
                    GetMagnetUrl(bturl);
                }
                Console.WriteLine("共有磁力链接:" + btlists.Count);
                btlistjson = JsonConvert.SerializeObject(btlists);
                currentAVMovie.MagnetUrl = btlistjson;
                Console.WriteLine("磁力链接:" + btlistjson);
                if (btlists.Count>0 )
                {
                    var avmoviedb = new BaseRepository<avmovie>();
                  if (avmoviedb.IsExist(a=>a.Code==currentAVMovie.Code))
                    {
                        avmoviedb.Update(n=>n.Code==currentAVMovie.Code,currentAVMovie);
                    }
                    else
                    {
                        avmoviedb.Add(currentAVMovie);
                    }
                }
                
                                              
            }
            
        }        
        private class MagenetDetailPipeline : BasePipeline
        {
            public override void Process(ResultItems resultItems)
            {
                btlists.Add(resultItems.Results["Magnet"]);
            }
        }
        private class MovieDetailPipeline : BasePipeline
        {
            public override void Process(ResultItems resultItems)
            {
                currentAVMovie = resultItems.Results["AVMovie"];
                BaseRepository<avmovie> b = new BaseRepository<avmovie>();
                
                if (downloadimage)
                {
                    
                    if (!File.Exists(@"images\" + currentAVMovie.Code + "logo.jpg"))
                    {
                        WebRequest wRequest = WebRequest.Create(currentAVMovie.LogoURL);
                        wRequest.ContentType = "image/jpeg";
                        wRequest.Method = "get";
                        var task = wRequest.GetResponseAsync();
                        WebResponse wResp = task.Result;
                        var s = wResp.GetResponseStream();
                        Image image = new Bitmap(s);
                        image.Save(@"images\" + currentAVMovie.Code + "logo.jpg");
                    }
                }
                
                if (b.GetCount(n => n.Code == currentAVMovie.Code) <= 0)
                {
                    b.Add(currentAVMovie);
                }
                else
                {
                    b.Update(currentAVMovie);
                }
                GetBTSOPW(currentAVMovie.Code);

            }
        }

        private class MovieListPipeline : BasePipeline
        {
            private static long count = 0;

            public override void Process(ResultItems resultItems)
            {
                Directory.CreateDirectory("images");
                BaseRepository<avmovie> b = new BaseRepository<avmovie>();
                foreach (avmovie entry in avMovies)
                {

                    GetMovieInfo(entry.MovieDetailUrl);
                }

                // 可以自由实现插入数据库或保存到文件
            }
        }
        #endregion


        #region 页面采集
        public class BTListProcessor : BasePageProcessor
        {
            protected override void Handle(Page page)
            {
                var n = 0;
                var btmagnetlist = page.Selectable.SelectList(Selectors.XPath(@".//div[@class='data-list']/div[@class='row']/a")).Links().Nodes();
                List<string> btmagneturls = new List<string>();
                foreach (var btmagnetdetail in btmagnetlist)
                {
                    Console.WriteLine("下载页地址:" + btmagnetdetail.GetValue());
                    n++;
                    btmagneturls.Add(btmagnetdetail.GetValue());
                    if (n>10)
                    { break; }
                }
                page.AddResultItem("BTURLS", btmagneturls);
            }
        }

        public class MagnetDetailProcessor : BasePageProcessor
        {
            protected override void Handle(Page page)
            {
              
                string mageneturl =
                  page.Selectable.Select(Selectors.XPath(@".//*[@id='magnetLink']")).GetValue();
                Console.WriteLine("磁力地址:" + mageneturl);
                page.AddResultItem("Magnet",mageneturl);
            }
        }
        public class MoviesDetailProcessor : BasePageProcessor
        {
            protected override void Handle(Page page)
            {

                string moviecode =
                    page.Selectable.Select(Selectors.XPath(@"html/body/div[2]/div[1]/div[2]/p[1]/span[2]")).GetValue();
                Console.WriteLine("代码:" + moviecode);
                var avmovie = avMovies.Find(n => n.Code == moviecode);
                page.AddResultItem("AVMovie", avMovies.Find(n => n.Code == moviecode));
                if (Selectors.XPath(@"html/body/div[2]/div[1]/div[2]/p[6]/a") != null)
                {
                    avmovie.Corp =
                    page.Selectable.Select(Selectors.XPath(@"html/body/div[2]/div[1]/div[2]/p[6]/a")).GetValue();
                    Console.WriteLine("公司：" + avmovie.Corp);
                }
                if (Selectors.XPath(@".//*[@id='avatar-waterfall']/a/span") != null)
                {
                    string avstarname=page.Selectable.Select(Selectors.XPath(@".//*[@id='avatar-waterfall']/a/span")).GetValue();
                    BaseRepository<avstar> a = new Demo.BaseRepository<Entity.avstar>();
                    if (a.IsExist(n=>n.JapaneseName.Contains(avstarname)))
                    {
                        avmovie.AvstarID = a.FindSingle(n => n.JapaneseName.Contains(avstarname)).Id;
                    }
                    else
                    {
                        avstar avstar = new avstar();
                        avstar.JapaneseName = avstarname;
                        a.Add(avstar);
                        avmovie.AvstarID = a.FindSingle(n => n.JapaneseName.Contains(avstarname)).Id;
                    }
                    Console.WriteLine("星星：" + avmovie.AvstarID);


                }
            }
        }
        public class MoviesPageProcessor : BasePageProcessor
        {
            protected override void Handle(Page page)
            {
                var totalVideoElements =
                    page.Selectable.SelectList(Selectors.XPath(@".//div[@class='item']")).Nodes();
                Console.WriteLine(totalVideoElements.Count);
                foreach (var videoElement in totalVideoElements)
                {

                    avmovie avMovie = new avmovie();
                    avMovie.MovieDetailUrl =
                        videoElement.Select(Selectors.XPath(@".//a[@class='movie-box']")).Links().GetValue();

                    avMovie.LogoURL =
                        videoElement.Select(Selectors.XPath(@".//div[@class='photo-frame']/img/@src")).GetValue();
                    avMovie.Code = videoElement.Select(Selectors.XPath(@".//div[@class='photo-info']/span/date[1]")).GetValue();
                    avMovie.PublisDateTime =
                        DateTime.Parse(
                            videoElement.Select(Selectors.XPath(@".//div[@class='photo-info']/span/date[2]")).GetValue());
                    
                    avMovies.Add(avMovie);
                }
                page.AddResultItem("MovieList", avMovies);

                foreach (var url in page.Selectable.SelectList(Selectors.XPath(".//a[@name='numbar']")).Links().Nodes())
                {
                    page.AddTargetRequest(new Core.Request(url.GetValue(), null));
                }
            }
        }
        #endregion

    }
}
